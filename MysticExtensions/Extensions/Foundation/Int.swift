//
//  Float.swift
//  iFocusTour
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension Int {
    
    // MARK: - Initializers
    public init(_ boolValue: Bool) {
        self = NSNumber(value: boolValue).intValue
    }
    
    // MARK: - Class Methods
    public static func random() -> Int {
        return self.random(Int.max)
    }
    
    public static func random(_ upperBound: Int) -> Int {
        return self.random(lowerBound: 0, upperBound: upperBound)
    }
    
    public static func random(lowerBound: Int, upperBound: Int) -> Int {
        var rand = Int(arc4random_uniform(UInt32(upperBound)))
        
        while rand < lowerBound || rand >= upperBound  {
            rand = Int(arc4random_uniform(UInt32(upperBound)))
        }
        
        return rand
    }
}
