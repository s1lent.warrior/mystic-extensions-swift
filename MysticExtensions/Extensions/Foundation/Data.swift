//
//  Data.swift
//  MysticExtensions
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension Data {
    
    // MARK: - Initializers
    public init?(_ fromFileInDocumentsDirectoryWithName: String, ofType fileType: String, isBase64Encoded: Bool) throws {
        let docsPath : NSString = NSString(string:"~/Documents").expandingTildeInPath as NSString
        let filePath = docsPath.appendingPathComponent(fromFileInDocumentsDirectoryWithName) + ".\(fileType)"
        let fileUrl = URL(fileURLWithPath: filePath)
        var fileContent : Data? = try Data(contentsOf: fileUrl)
        if isBase64Encoded {
            if let content = fileContent {
                fileContent = Data(base64Encoded: content, options: .ignoreUnknownCharacters)
            }
        }
        
        if fileContent == nil {
            return nil
        } else {
            self = fileContent!
        }
        
    }
    
    // MARK: - Class Methods
    public static func fromFileInDocumentsDirectory(withName fileName: String, ofType fileType: String) throws -> Data? {
        return try Data(fileName, ofType: fileType, isBase64Encoded: false)
    }
    
    public static func fromFileInDocumentsDirectory(withName fileName: String, ofType fileType: String, isBase64Encoded: Bool) throws -> Data? {
        return try Data(fileName, ofType: fileType, isBase64Encoded: isBase64Encoded)
    }
    
    // MARK: - Instance Methods
    public func writeToDocumentsDirectory(_ fileName: String, ofType fileType: String) throws -> String {
        return try self.writeToDocumentsDirectory(fileType, ofType: fileType, isBase64Encoded: false)
    }
    
    public func writeToDocumentsDirectory(_ fileName: String, ofType fileType: String, isBase64Encoded: Bool) throws -> String {
        let docsPath : NSString = NSString(string:"~/Documents").expandingTildeInPath as NSString
        let filePath = docsPath.appendingPathComponent(fileName) + ".\(fileType)"
        
        var data = self
        if isBase64Encoded {
            data = self.base64EncodedData(options: .endLineWithCarriageReturn)
        }
        
        try data.write(to: URL(fileURLWithPath: filePath as String), options: [.atomic])
        return filePath
    }
}
