//
//  Bool.swift
//  iFocusTour
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension Bool {
    
    // MARK: - Initializers
    public init(_ intValue: Int) {
        self = NSNumber(value: intValue).boolValue
    }
    
    public init(_ floatValue: Float) {
        self = NSNumber(value: floatValue).boolValue
    }
    
    public init(_ doubleValue: Double) {
        self = NSNumber(value: doubleValue).boolValue
    }
}
